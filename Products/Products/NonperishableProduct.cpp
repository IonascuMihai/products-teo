#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(int32_t id, const std::string& name, float rawPrice, NonperishableProductType type) :Product(id, name, rawPrice)
{
	m_type = type;
}

#pragma region Getters
NonperishableProductType NonperishableProduct::getType() const
{
	return m_type;
}
float NonperishableProduct::getPrice() const
{
	return m_rawPrice + static_cast<float>(getVAT())* m_rawPrice / 100.0f;
}
int32_t NonperishableProduct::getVAT() const
{
	return 19;
}
#pragma endregion Getters
