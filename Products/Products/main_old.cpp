//#include <iostream>
//#include <fstream>
//#include <vector>
//#include <algorithm>
//#include "Product.h"
//
//#pragma region Phase1
//std::vector<Product> readProducts(std::string filename)
//{
//	std::ifstream productFile(filename);
//	std::vector<Product> products;
//
//	while (!productFile.eof())
//	{
//		uint16_t id;
//		std::string name;
//		float price;
//		uint16_t vat;
//		std::string dateOrType;
//
//		productFile >> id >> name >> price >> vat >> dateOrType;
//		products.emplace_back(Product(id, name, price, vat, dateOrType));
//	}
//
//	productFile.close();
//	return products;
//}
//
//void printProducts(const std::vector<Product>& products)
//{
//	for (const auto& p : products)
//		std::cout << p.getId() << " " << p.getName() << " " << p.getPrice() << " " << p.getVat() << " " << p.getDateOrType() << "\n";
//}
//
//void printNonPerishableProducts(const std::vector<Product>& products)
//{
//	for (const auto& p : products)
//	{
//		if (!p.isPerishable())
//			std::cout << p.priceWithVAT() << "\n";
//	}
//}
//
//bool nameCompare(const Product& p1, const Product& p2)
//{
//	return p1.getName() < p2.getName();
//}
//
//bool priceCompare(const Product& p1, const Product& p2)
//{
//	return p1.getPrice() < p2.getPrice();
//}
//
//void sortByName(std::vector<Product>& products)
//{
//	std::sort(products.begin(), products.end(), nameCompare);
//}
//
//void sortByPrice(std::vector<Product>& products)
//{
//	std::sort(products.begin(), products.end(), priceCompare);
//}
//
//void printSortedProducts(std::vector<Product>& products)
//{
//	std::cout << "How would you like your products to be sorted?\n1 - By Name\n2 - By Price\n Other - Unsorted\n";
//	char option;
//	std::cin >> option;
//	switch (option)
//	{
//	case '1':
//		sortByName(products);
//		break;
//	case '2':
//		sortByPrice(products);
//		break;
//	default:
//		break;
//	}
//	printProducts(products);
//}
//#pragma endregion Phase1
//
//int main()
//{
//	std::vector<Product> products = readProducts("Products.prodb");
//	printNonPerishableProducts(products);
//	printSortedProducts(products);
//	
//	return 0;
//}