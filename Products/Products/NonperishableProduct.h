#pragma once
#include "Product.h"

enum class NonperishableProductType
{
	Clothing=1,
	SmallAppliences=Clothing<<1,
	PersonalHygiene=SmallAppliences<<1
};

class NonperishableProduct :public Product
{
	
public:
	NonperishableProduct(int32_t id, const std::string& name, float rawPrice, NonperishableProductType type);
	~NonperishableProduct() override = default;

	NonperishableProductType getType() const;
	float getPrice() const override final;
	int32_t getVAT() const override final;

private:
	NonperishableProductType m_type;
};